//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Danni Brito on 10/1/18.
//  Copyright © 2018 Danni Brito. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var cosaTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var notificationMessage = "El texto es: "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //Para recuperar
        //1. instanciar userdefaults
        let defaults = UserDefaults.standard
        //2. Acceder al valor por medio del KEY
        resultLabel.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.sound,.alert]) { (granted, error) in
            
        }
        
        sendNotification()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func botonButton(_ sender: Any) {
        resultLabel.text = cosaTextField.text
        
        //para guardar
        //1. instanciar UserDefaults
        let defaults = UserDefaults.standard
        //2. Guardar variable en defaults
        //Int, Bool, Float, Double, String, Object!
        defaults.set(cosaTextField.text, forKey: "label")
        
    }
    
    func sendNotification(){
        notificationMessage += resultLabel.text!
        //1. Authorization Request (en viewDidLoad)
        //2. Crear contenido de la notificación
        
        let content = UNMutableNotificationContent()
        content.title = "Notification Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        
        //3. definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        //4. definir un identifier para la notificacion
        
        let identifier = "notification"
        
        //5. crear request
        
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        //6. añadir el request al usernotificationcenter
        
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            
        }
        
        
    }


}

